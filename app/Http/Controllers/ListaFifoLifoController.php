<?php

namespace App\Http\Controllers;

use SplDoublyLinkedList;

use Illuminate\Http\Request;

class ListaFifoLifoController extends Controller
{
    public function listaFifoLifo(){
    
        $Lista = new SplDoublyLinkedList;
        $Lista->setIteratorMode(SplDoublyLinkedList::IT_MODE_LIFO);

        $Lista->push(1);
        $Lista->push(2);
        $Lista->push(3);
        $Lista->push(4);
        $Lista->push(5);

        //print_r($Lista)
        echo "<h2>Lista LIFO (PILA)</h2>";

        $Lista-> rewind();
        while ($Lista->valid()) {
            echo $Lista->current()."<br>";
            $Lista->next();
        }

        echo "<h2>Lista FIFO (COLA)</h2>";
        $Lista->setIteratorMode(SplDoublyLinkedList::IT_MODE_FIFO);
        $Lista-> rewind();
        while ($Lista->valid()) {
            echo $Lista->current()."<br>";
            $Lista->next();
        }
        
    }   
}
