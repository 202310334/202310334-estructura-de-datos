<?php

namespace App\Http\Controllers;

use SplDoublyLinkedList;

use Illuminate\Http\Request;

class ListaInicioFinController extends Controller
{
    public function listaInicioFin(){
        $Lista = new SplDoublyLinkedList;

        $Lista->push(1);
        $Lista->push(2);
        $Lista->push(3);
        $Lista->push(4);
        $Lista->push(5);

        
        echo $Lista->top()."<br>";
        echo $Lista->bottom()."<br>";

    }
}
