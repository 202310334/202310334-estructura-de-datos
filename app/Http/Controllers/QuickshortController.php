<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuickshortController extends Controller
{
    public function quick_sort($arr){

        if(count($arr) <= 1){
            return $arr;   
        }
        else{
            $Pivote = $arr[0];
            $Izquierda = [];
            $Derecha = [];
        }
        for($i = 1; $i < count($arr); $i++)
        {
            if($arr[$i] < $Pivote){
                $Izquierda[] = $arr[$i];
            }
            else{
                $Derecha[] = $arr[$i];
            }
        }
        echo "<br><br>";
        echo implode(",",$Izquierda)."Izquierda<br>";
        print_r($Pivote);
        echo "Pivote <br>";
        echo implode(",",$Derecha)." Derecha<br>";

        return array_merge($this->quick_sort($Izquierda), array($Pivote), $this->quick_sort($Derecha));
    }

    public function OrdenamientoQuickShort(){
        $arreglo = [5,3,8,6,2,7];
        echo implode(",",$arreglo)." Antes<br>";
        $odenamiento = $this->quick_sort($arreglo);
        echo implode(",",$odenamiento)." Despuess <br>";
    }
}
