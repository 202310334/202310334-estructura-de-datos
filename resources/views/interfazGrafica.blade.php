<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    
    
</head>
<body>
  <div class="container">
    <div class="col-6">
    <h1>Visorias de futbol de 18 a 25 años.</h1>
    </div>
  </div>

  <div class="container">
    <div class="col-6">
      <h4>Completar los espacios para participar.</h4>
    </div>
  </div>

  <div class="container">
  <form class="row g-3">
  <div class="col-md-6">
    <label for="inputEmail4" class="form-label">NOMBRES</label>
    <input type="NOMBRES" class="form-control" id="inputEmail4">
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label">APELLIDOS</label>
    <input type="APELLIDOS" class="form-control" id="inputAPELLIDOS">
  </div>
  
  <div class="col-md-6">
    <label for="inputEDAD" class="form-label">CORREO ELECTRONICO</label>
    <input type="text" class="form-control" id="inputEDAD">
  </div>

  <div class="col-md-4">
    <label for="inputEDAD" class="form-label">NUMERO DE TELEFONO</label>
    <input type="text" class="form-control" id="inputEDAD">
  </div>

  <div class="col-md-2">
    <label for="inputPESO" class="form-label">EDAD</label>
    <input type="text" class="form-control" id="inputPESO">
  </div>
</form>

<h4>SEXO</h4>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
  <label class="form-check-label" for="flexCheckDefault">
    Masculino
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
  <label class="form-check-label" for="flexCheckChecked">
    Femenino
  </label>
</div>


  <form class="row g-3">
  <div class="col-md-2">
    <label for="inputEmail4" class="form-label">PESO</label>
    <input type="NOMBRES" class="form-control" id="inputEmail4">
  </div>

  <div class="col-md-2">
    <label for="inputPassword4" class="form-label">ALTURA</label>
    <input type="APELLIDOS" class="form-control" id="inputAPELLIDOS">
  </div>
</form>

<h4>POSICION DE JUEGO (Seleccione mas opciones en caso de que juegue varias posiciones)</h4>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
  <label class="form-check-label" for="flexCheckIndeterminate">
    PORTERO
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
  <label class="form-check-label" for="flexCheckIndeterminate">
    DEFENSA
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
  <label class="form-check-label" for="flexCheckIndeterminate">
    MEDIO CAMPO
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
  <label class="form-check-label" for="flexCheckIndeterminate">
    DELANTERO
  </label>
</div>

<div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">EXPLIQUE SUS ABILIDADES EN EL TERRENO DE JUEGO
  </label>
  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
</div>

<p class="text-end">Una vez envidado el formulario espere un tiempo de 24 horas para poder tomar su informacion
  y mediante de su correo electronico o un mensaje a su numero de telefono se le hara saber su cita programada.
</p>

<button type="button" class="btn btn-primary">Enviar formulario</button>
<button type="button" class="btn btn-danger">Cacelar Formulario</button>

</body>
</html>   