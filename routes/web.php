<?php

use App\Http\Controllers\QuickshortController;
use App\Http\Controllers\BurbujaController;
use App\Http\Controllers\ArreglosController;
use App\Http\Controllers\LoginArrayController;
use App\Http\Controllers\ListaFifoLifoController;
use App\Http\Controllers\ListaInicioFinController;
use App\Http\Controllers\AgregarListaIndiceController;
use App\Http\Controllers\InterfazGraficaController;
use App\Http\Controllers\RecursividadController;
use App\Http\Controllers\MemoriaController;
use App\Http\Controllers\ApuntadoresController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ordenamiento-QuickShort', [QuickshortController::class, 'OrdenamientoQuickShort']);
Route::get('/ordenamiento-burbuja', [BurbujaController::class, 'OrdenamietoBurbuja']);
Route::get('/practica-arreglos', [ArreglosController::class, 'Arreglos']);
Route::get('/lista-fifo-lifo', [ListaFifoLifoController::class, 'listaFifoLifo']);
Route::get('/lista-inicio-fin', [ListaInicioFinController::class, 'listaInicioFin']);
Route::get('/agregar-lista-indice', [AgregarListaIndiceController::class, 'Agregarlista']);
Route::get('/apuntadores', [ApuntadoresController::class, 'Apuntadores']);
Route::get('/memoria', [MemoriaController::class, 'Memoria']);
Route::get('/Recursividad', [RecursividadController::class, 'Incrementable']);
Route::get('/InterfazGrafica', [InterfazGraficaController::class, 'Inicio']);
Route::get('/Login', [LoginArrayController::class, 'InicioLogin']);
Route::post('/Login_Validacion', [LoginArrayController::class, 'LoginValidacion']);